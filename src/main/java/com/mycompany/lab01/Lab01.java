/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;
import java.util.Scanner;
/**
 *
 * @author PT
 */
public class Lab01 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int n = 3; // make bord 3*3
        char[][] board = new char[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                board[i][j] = '-';
            }
            System.out.println();
    }
    System.out.println("welcome to OX");
    String p1 = "X";
    String p2 = "O";
    boolean player1 = true;
    boolean gameEnded = false;
while(!gameEnded) {
    //drawBoard
    drawBoard(board);
    //print who turn
    if(player1) {
        System.out.println("X" + " Turn");
    } else {
        System.out.println("O" + " Turn");
    }
    // creat char keep 'x' or 'o' 
    char c = '-';
    if(player1) {
        c = 'x';
    } else {
        c = 'o';
    }
    int num = 0;
    int row = 0;
    int col = 0;
    while(true) {
        System.out.print("Enter a number: ");num = kb.nextInt(); //ใส่เลข
        //col and row start 0
        if (num==1) { //num1=row2col0
            row = 2;
            col = 0;
        }if (num==2) { //num2=row2col1
            row = 2;
            col = 1;
        }if (num==3) { //num3=row2col2
            row = 2;
            col = 2;
        }if (num==4) { //num4=row1col0
            row = 1;
            col = 0;
        }if (num==5) { //num5=row1col1
            row = 1;
            col = 1;
        }if (num==6) { //num6=row1col2
            row = 1;
            col = 2;
        }if (num==7) { //num7=row0col0
            row = 0;
            col = 0;
        }if (num==8) { //num8=row0col1
            row = 0;
            col = 1;
        }if (num==9) { //num9=row0col2
            row = 0;
            col = 2;
        }
        if(num>9) {
        System.out.println("This position is off the bounds of the board");
        
        } else if(board[row][col] != '-') {
        System.out.println("Someone has already made a move at this position");
        } else {
        break;}
    }
    //fix col
    board[row][col] = c;
    //examine who win
    if(playerHasWon(board) == 'x') {
        System.out.println("Congratulations");
        System.out.println("X" + " win");
        gameEnded = true;
    } else if(playerHasWon(board) == 'o') {
        System.out.println("Congratulations");
        System.out.println("O" + " win");
        gameEnded = true;
    } else {
        //no one win maybe tie
        if(boardIsFull(board)) {
        System.out.println("It's a tie!");
        gameEnded = true;
    } else 
    //switch turn
    {player1 = !player1;}
  }
}
drawBoard(board);//drawBoard when end
}//drawBoard OX
public static void drawBoard(char[][] board) {
System.out.println("Board:");
for(int i = 0; i < board.length; i++) {
    //loop in row
    for(int j = 0; j < board[i].length; j++) {
        System.out.print(board[i][j]);
    }
    System.out.println(); //make row in new line
   }
}

//who win return character
public static char playerHasWon(char[][] board) {
//examine row1
for(int i = 0; i < board.length; i++) {
    boolean inARow = true;
    char value = board[i][0];
if(value == '-') { 
    inARow = false;
//examine row2
} else {
    for(int j = 1; j < board[i].length; j++) {
        if(board[i][j] != value) {
            inARow = false;
            break;}
        }
    }

if(inARow) {
return value;
}
}

//examine col1
for(int j = 0; j < board[0].length; j++) {
boolean inACol = true;
char value = board[0][j];

if(value == '-') {
inACol = false;
} else {
//examine col2
for(int i = 1; i < board.length; i++) {
if(board[i][j] != value) {
inACol = false;
break;
}
}
}

if(inACol) {

return value;
}
}

//examine tri
//examine tri1: [0][0], [1][1], [2][2]...
boolean inADiag1 = true;
char value1 = board[0][0];
if(value1 == '-') {
inADiag1 = false;
} else {
for(int i = 1; i < board.length; i++) {
if(board[i][i] != value1) {
inADiag1 = false;
break;
}
}
}

if(inADiag1) {
return value1;
}

//examine tri2: [0][n-1], [1][n-2], [2][n-3]...
boolean inADiag2 = true;
char value2 = board[0][board.length-1];

if(value2 == '-') {
inADiag2 = false;
} else {
for(int i = 1; i < board.length; i++) {
if(board[i][board.length-1-i] != value2) {
inADiag2 = false;
break;
}
}
}

if(inADiag2) {
return value2;
}

//no one win
return ' ';
}

//examine bord full
public static boolean boardIsFull(char[][] board) {
for(int i = 0; i < board.length; i++) {
for(int j = 0; j < board[i].length; j++) {
if(board[i][j] == '-') {
return false;
}
}
}
return true;
  }
}
